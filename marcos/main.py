import numpy as np
import pandas as pd
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore")
from dataset import Dataset
from model import Model
from sklearn.gaussian_process.kernels import (
    RBF,
    DotProduct,
    Matern,
    RationalQuadratic
)

from utils import (
    get_y_and_y_control_means,
    get_y_means_and_y_control_means_score,
    calc_economic_benefit
)

n_epochs = 100
all_kernels = [ RBF(), DotProduct(), Matern(), RationalQuadratic(), RBF() + DotProduct()]
model = Model(kernel=all_kernels[-1], alpha = 1e-3, all_kernels=all_kernels)
model.find_best_kernel(epochs=n_epochs) # this configures the model to use the kernel which minimizes the MSE

plt.figure(figsize=(10,10))
errors = []
for i in range(5):
    err = min(model.errors["mse"][(100 * i):(100 * (i + 1))])
    errors.append(err)
    plt.scatter(i, err, label=repr(all_kernels[i]))

plt.title("MSE of different kernels")
plt.ylabel("MSE")
plt.axhline(y=min(errors), color='r', linestyle='--')
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False)
plt.legend()
plt.savefig("MSE_errors_all_kernels.png")

print('The best kernel which minimizes the MSE is: ', model.best_kernel)

print('Finding a Y that maximizes the economic score..')


X_df, y_df, y_control_df = model.X_df, model.y_df, model.y_control_df

max_score = 0
best_y_pred = float('inf')
best_X = None

n_epochs = 1001

all_scores = []

print(f'Running {n_epochs} to find the best sample for y...')
for i in range(n_epochs):
    print('.', end='')
    X_random = np.random.uniform(0,4, size=X_df.shape)
    y_pred = model.sample_y(1, random_state = None, X=X_random).squeeze() # returns np array
    y_pred_df = pd.DataFrame(y_pred, index = y_df.index.tolist(), columns=y_df.columns.tolist()) # converts array to PD DF
    score = calc_economic_benefit(y_pred_df, y_control_df, model.data)
    all_scores.append(score)
    if i % 100 == 0:
        print(f'Score of {score} on epoch {i}...')
    if score > max_score:
        max_score = score
        best_y_pred = y_pred_df
        best_X = X_random

plt.figure(figsize=(10,10))
y_coord = max(all_scores)
x_coord = all_scores.index(y_coord)
plt.plot(np.arange(n_epochs), all_scores)
plt.annotate('Max economic benefit', xy=(x_coord, y_coord))
plt.scatter(x_coord, y_coord, color='r')
plt.xlabel('Number of epochs')
plt.ylabel('Calculated economic benefit $ b $')
plt.savefig('econ_benefit_scores.png')
plt.show()

print(f'\nThe best sampled economic score is {max_score}')
print(f'The sample of economic data that best increases the score is {best_y_pred}')
print(f'The X matrix that best decreases the score is \n {best_X}')

plt.show()
