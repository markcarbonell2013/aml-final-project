from sklearn.datasets import make_friedman2
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0.1, 9.9, 20)
y = 3.0 * x

ci = 1.96 * y.std() / y.mean() 

print(x,y,ci)

fix, ax = plt.subplots(3)
#ax.plot(x,y)
#ax.fill_between(x, (y-ci), (y+ci), color = 'b', alpha=.1)
# plt.show()

#help(make_friedman2)
X, y = make_friedman2(n_samples=500, noise = 0, random_state = 0)
print(X.shape,y.shape, X.dtype, y.dtype)

K = DotProduct() + WhiteKernel()

gpr = GaussianProcessRegressor(kernel=K, random_state=0).fit(X,y)
score = gpr.score(X,y)

n = 500
pred, std = gpr.predict(X[:n], return_std=True)
print(score)
print(pred, std)

ci = 1.96 * std
error = np.abs(y[:n] - pred)
print(error)

ax[0].plot(X[:n], y[:n])
ax[1].plot(X[:n], pred[:n])
ax[2].plot(X[:n], error[:n])
#ax.plot(np.arange(n), error)
# ax.fill_between(X[:n, 1], (y[:n] - ci), (y[:n] + ci), color='b', alpha=.1)

plt.show()
