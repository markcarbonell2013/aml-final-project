
import os
import numpy as np
import matplotlib.pyplot as plt
from dataset import Dataset
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import (
    RBF,
    DotProduct,
    Matern,
    RationalQuadratic,
    ExpSineSquared,
)

class Model():
    def __init__(self, kernel, alpha = 1e-3, data_path = 'data', saved_data_path = 'saved_data', all_kernels=[]):
        self.data_path = data_path
        self.saved_data_path = saved_data_path
        self.data = Dataset(data_path, saved_data_path)
        self.kernel = kernel
        self.all_kernels = all_kernels
        self.best_kernel = kernel
        self.alpha = alpha
        self.model = GaussianProcessRegressor(kernel=self.kernel, alpha=self.alpha, random_state=0)
        self.X_df, self.y_df, self.y_control_df = self.data.get_X_y(renew=False, as_numpy=False, get_econ_control=True)
        self.X, self.y, self.y_control = self.data.get_X_y(renew=False, as_numpy=True, get_econ_control=True) 
        self.y_pred = None
        self.ci_95 = None
        self.errors = {
            "mse": [],
            "abs": []
        }

    def fit(self):
        self.model.fit(self.X, self.y)

    def get_score(self):
        self.fit()
        score = self.model.score(self.X, self.y)
        return score

    def predict(self, X):
        self.y_pred, std = self.model.predict(X, return_std=True)
        self.ci_95 = 1.96 * std
        self.errors["mse"].append(np.square(self.y - self.y_pred).mean())
        self.errors["abs"].append(np.abs(self.y - self.y_pred).sum())
        return self.y_pred

    def sample_y(self, n_samples, random_state=0, X = None):
        self.X = self.X if X is None else X
        self.fit()
        sampled_y = self.model.sample_y(self.X, n_samples)
        return sampled_y

    def find_best_kernel(self, epochs=100):
        for kernel in self.all_kernels:
            print(f'Training kernel {kernel}...')
            for epoch in range(epochs):
                self.model = GaussianProcessRegressor(kernel=kernel, alpha=self.alpha, random_state=None)
                self.fit()
                self.predict(self.X)
                print('.', end='')
            print(' ')
                
        best_idx = self.errors["mse"].index(min(self.errors["mse"]))
        self.best_kernel = self.all_kernels[best_idx // epochs]

        return self.best_kernel

    def plot(self):
        n_plots = 3
        fig, ax = plt.subplots(n_plots)
        ax[0].plot(self.X, self.y)
        ax[0].title('True y')
        ax[1].plot(self.X, self.pred_y)
        ax[1].title('Predicted y')
        colors = ['r', 'g', 'b', 'r']
        for k in range(self.X.shape[1]):
            ax[2].fill_between(self.X[:,k], self.y[:,k] - self.ci_95, self.y[:,k] + self.ci_95, color=colors[k], alpha=.1)
            ax[2].plot(self.X[:,k],self.y[:,k])
        plt.show()

