
import os
import pandas as pd
import numpy as np
from glob import glob

class Dataset():
    def __init__(self, data_path = 'data', saved_data_path = 'saved_data'):
        self.data_path = data_path
        self.saved_data_path = os.path.join('.',saved_data_path)
        self.econ_path = os.path.join(data_path, 'economics')
        self.poli_path = os.path.join(data_path, 'policies')
        self.econ = {}
        self.poli = {}

        self.econ_effects = {
            "Business_investment_rate": "positive",                                     
            "Business_profit_share": "positive",                                        
            "Employment_growth_quarterly": "positive",                                  
            "GDP_growth_quarterly": "positive",                                         
            "Government_debt_quarterly": "negative",                                    
            "Government_deficit_surplus_quarterly": "positive",                         
            "Household_investment_rate_quarterly": "positive",
            "House_prices_quarterly": "negative",
            "Job_vacancy_rate_quarterly": "negative",
            "Labour_costs_index_quarterly": "negative",
            "Labour_productivity_quarterly": "positive",
            "Nominal_unit_labour_costs_quarterly": "negative",
        }

    def load_data_for_tests(self, renew=False):
        _,_ = self.load_data(renew=renew)
        return self

    def load_data(self, renew=False): 
        if os.path.isdir(self.saved_data_path) is True and renew is False:
            self.load_dfs_from_saved_data()
        else:
            self.load_dfs_from_original_data()
            self.format_econ_dfs()
            self.format_poli_dfs()
            if renew is True:
                self.save_dfs()
        return self.econ, self.poli

    def load_dfs_from_saved_data(self):
        assert os.path.isdir(self.saved_data_path), "Saved data cannot be loaded if it does not exist"
        econ_path = os.path.join(self.saved_data_path, 'economics')
        for f in glob(os.path.join(econ_path, '*')):
            k = os.path.basename(f)
            self.econ[k] = pd.read_csv(f)

        poli_path = os.path.join(self.saved_data_path, 'policies')
        for f in glob(os.path.join(poli_path, '*')):
            k = os.path.basename(f)
            self.poli[k] = pd.read_csv(f)
        return self

    def load_dfs_from_original_data(self):
        for f in glob(os.path.join(self.econ_path, '*')):
            filename = os.path.basename(f)
            df = pd.read_excel(f, sheet_name=2, index_col=0, engine='openpyxl', header=None)
            df.columns.name = df.loc['Unit of measure', 2] if any(df.index == 'Unit of measure') else df.loc['Employment indicator', 2]
            self.econ[filename] = df

        for f in glob(os.path.join(self.poli_path, '*')):
            filename = os.path.basename(f)
            df = pd.read_csv(f, header=None)
            self.poli[filename] = df
        return self

    def format_econ_dfs(self):
        for k in self.econ.keys():
            df = self.econ[k]
            df = df['TIME':].iloc[:, ::2].dropna(how='any').replace(':', np.nan)
            df = df.rename(index={'Germany (until 1990 former territory of the FRG)': 'Germany', 'Czechia': 'Czech Republic', 'Slovakia': 'Slovak Republic'})
            df = df.drop(index='North Macedonia') if 'North Macedonia' in df.index else df
            df = df.drop(index='Euro area - 19 countries  (from 2015)') if 'Euro area - 19 countries  (from 2015)' in df.index else df
            df = df.drop(df.index[1:3])
            df.columns = df.iloc[0,:]
            df = df.drop(df.index[0])
            df = df.rename(index={'TIME': 'country_name'})
            df.index.name, df.columns.name = None, None
            df.insert(0, 'country_name', df.index)
            df = df.reset_index(drop=True)
            self.econ[k] = df
        return self

    def format_poli_dfs(self):
        for k in self.poli.keys():
            df = self.poli[k]
            df = df.drop(columns=0)
            df.columns = df.iloc[0,:]
            df = df.drop(0)
            df.columns.name, df.index.name = None,None
            df = df.reset_index(drop=True)
            cols = df.columns[df.columns.get_loc('01Jan2020'):df.columns.get_loc('03Aug2021')]
            df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')
            df['2020-Q1'] = df.loc[:, '01Jan2020':'31Mar2020'].mean(axis=1, numeric_only=True,skipna=True)
            df['2020-Q2'] = df.loc[:, '01Apr2020':'30Jun2020'].mean(axis=1, numeric_only=True,skipna=True)
            df['2020-Q3'] = df.loc[:, '01Jul2020':'30Sep2020'].mean(axis=1, numeric_only=True,skipna=True)
            df['2020-Q4'] = df.loc[:, '01Oct2020':'31Dec2020'].mean(axis=1, numeric_only=True,skipna=True)
            df['2021-Q1'] = df.loc[:, '01Jan2021':'31Mar2021'].mean(axis=1, numeric_only=True,skipna=True)
            df['2021-Q2'] = df.loc[:, '01Apr2021':'30Jun2021'].mean(axis=1, numeric_only=True,skipna=True)
            df['2021-Q3'] = df.loc[:, '01Jul2021':           ].mean(axis=1, numeric_only=True, skipna=True)
            df = df.drop(columns=df.columns[2:df.columns.get_loc("03Aug2021")+1])
            df = df.drop(columns=["country_code"])
            df.loc[:, '2020-Q1':] = df.loc[:, '2020-Q1':].fillna(0).round(0).astype(np.int64)
            self.poli[k] = df
        return self

    def save_dfs(self):
        econ_path = os.path.join(self.saved_data_path, 'economics')
        poli_path = os.path.join(self.saved_data_path, 'policies')
        if not os.path.isdir(self.saved_data_path):
            os.mkdir(self.saved_data_path)
            os.mkdir(econ_path)
            os.mkdir(poli_path)

        for k,df in self.econ.items():
            f_path = os.path.join(econ_path, k)
            df.to_csv(f_path)

        for k,df in self.poli.items():
            f_path = os.path.join(poli_path, k)
            df.to_csv(f_path)

    def show_filenames(self):
        for k,df in self.econ.items():
            print(len(self.econ.keys()), k, df.shape)
            print(df)

        for k,df in self.poli.items():
            print(len(self.poli.keys()), k, df.shape)
            print(df)

    def get_X_y(self, renew=False, as_numpy=False, get_econ_control=False):
        econ, poli = self.load_data(renew=renew)

        poli_df = pd.DataFrame()
        econ_df = pd.DataFrame()

        col = None
        for k,df in poli.items():
            fname = k[3:]
            fname = fname[:-4]
            col = df.loc[:,'country_name']
            df = df.drop(columns=['country_name'])
            df = df.add_suffix(f'-{fname}')
            poli_df = pd.concat((poli_df, df), axis=1)
        poli_df = poli_df.set_index(col)

        for k,df in econ.items():
            fname = k[:-5]
            col = df.loc[:,'country_name']
            df = df.drop(columns=['country_name'])
            df = df.add_suffix(f'-{fname}')
            econ_df = pd.concat((econ_df, df), axis=1)
        econ_df = econ_df.iloc[:-3, :]
        econ_df = econ_df.set_index(col, drop=True)

        econ_df_control = econ_df.fillna(0).filter(regex=r'^201[89]-Q', axis=1).sort_index(axis=0,ascending=True).astype(np.float64)

        poli_df = poli_df.fillna(0).filter(regex=r'^202[01]-Q[1234]', axis=1).sort_index(axis=0, ascending=True).astype(np.float64)
        econ_df = econ_df.fillna(0).filter(regex=r'^202[01]-Q[1234]', axis=1).sort_index(axis=0, ascending=True).astype(np.float64)

        Xy = poli_df.merge(econ_df, on='country_name')

        poli_df = Xy[[c for c in Xy.columns if c in poli_df.columns.tolist()]]
        econ_df = Xy[[c for c in Xy.columns if c in econ_df.columns.tolist()]]

        assert poli_df.shape == poli_df.to_numpy().shape
        assert econ_df.shape == econ_df.to_numpy().shape
        assert econ_df_control.shape == econ_df_control.to_numpy().shape

        if as_numpy is True:
            poli_df = poli_df.to_numpy()
            econ_df = econ_df.to_numpy()
            econ_df_control = econ_df_control.to_numpy()

        if get_econ_control is True:
            return poli_df, econ_df, econ_df_control

        return poli_df, econ_df
