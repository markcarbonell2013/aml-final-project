
import shutil
import os
import unittest
unittest.TestLoader.sortTestMethodsUsing = None
import pandas as pd

from glob import glob
from dataset import Dataset

class TestDatasetOriginalData(unittest.TestCase):
    def setUp(self):
        saved_data = 'saved_test_data'
        if os.path.isdir(saved_data):
            shutil.rmtree(saved_data)
        self.dataset = Dataset('data', saved_data).load_data_for_tests()

    def tearDown(self):
        if os.path.isdir(self.dataset.saved_data_path):
            shutil.rmtree(self.dataset.saved_data_path)

    def test_get_X_y(self):
        X, y = self.dataset.get_X_y()
        n_quartals = 7
        n_econ = len(self.dataset.econ.keys())
        n_poli = len(self.dataset.poli.keys())
        self.assertTrue(len(X.columns) == n_poli * n_quartals)
        self.assertTrue(len(y.columns) == n_econ * n_quartals)
        self.assertTrue(all(X.index == y.index))

    def test_important_countries_are_included_in_both_dfs(self):
        econ_countries, poli_countries = set(),set()
        [econ_countries.update(df.loc[:,'country_name'].tolist()) for _,df in self.dataset.econ.items()]
        [poli_countries.update(df.loc[:,'country_name'].tolist()) for _,df in self.dataset.poli.items()]
        self.assertTrue(econ_countries.issubset(poli_countries))


    def test_amount_of_files_and_keys(self):
        e_fs = glob(os.path.join(self.dataset.econ_path, '*'))
        p_fs = glob(os.path.join(self.dataset.poli_path, '*'))
        self.assertEqual(len(self.dataset.econ.keys()), len(e_fs))
        self.assertEqual(len(self.dataset.poli.keys()), len(p_fs))

    def test_all_files_and_keys_are_correctly_defined(self):
        e_fs = [os.path.basename(p) for p in glob(os.path.join(self.dataset.econ_path, '*'))]
        p_fs = [os.path.basename(p) for p in glob(os.path.join(self.dataset.poli_path, '*'))]
        for ek,pk in zip(self.dataset.econ.keys(), self.dataset.poli.keys()):
            self.assertTrue(ek in e_fs)
            self.assertTrue(pk in p_fs)
            self.assertTrue(isinstance(self.dataset.econ[ek], pd.DataFrame))
            self.assertTrue(isinstance(self.dataset.poli[pk], pd.DataFrame))

    def test_dfs_have_quartal_and_country_columns(self):
        cols = ['2020-Q1','2020-Q2','2020-Q3','2020-Q4','2021-Q1', 'country_name'] # are not present in the econ data'2021-Q2','2021-Q3'
        for c in cols:
            try:
                q_in_econ_dfs = [any(df.columns == c) for _,df in self.dataset.econ.items()]
                q_in_poli_dfs = [any(df.columns == c) for _,df in self.dataset.poli.items()]
                self.assertTrue(all(q_in_econ_dfs))
                self.assertTrue(all(q_in_poli_dfs))
            except AssertionError as e:
                print(c)
                print(q_in_econ_dfs, q_in_poli_dfs)
                tups = [(k,df) if any(df.columns == c) is False else None for k,df in self.dataset.econ.items()]
                print(tups)
                raise AssertionError(e)

    def test_save_dfs(self):
        if os.path.isdir(self.dataset.saved_data_path):
            shutil.rmtree(self.dataset.saved_data_path)
        self.assertFalse(os.path.isdir(self.dataset.saved_data_path))

        self.dataset.save_dfs()

        self.assertTrue(os.path.isdir(self.dataset.saved_data_path))
        econ_path = os.path.join(self.dataset.saved_data_path, 'economics')
        poli_path = os.path.join(self.dataset.saved_data_path, 'policies')
        self.assertTrue(os.path.isdir(econ_path))
        self.assertTrue(os.path.isdir(poli_path))

    def test_load_dfs_from_csv(self):
        self.dataset.save_dfs()
        self.dataset = Dataset(self.dataset.data_path, self.dataset.saved_data_path).load_dfs_from_saved_data()
        self.test_dfs_have_quartal_and_country_columns()
        self.test_important_countries_are_included_in_both_dfs()
        self.test_amount_of_files_and_keys()
        self.test_all_files_and_keys_are_correctly_defined()
        self.test_save_dfs()

    def test_load_data_with_renewal(self):
        econ, poli = self.dataset.load_data(renew=True)
        self.assertTrue(isinstance(econ, dict))
        self.assertTrue(isinstance(poli, dict))
        self.assertTrue(all(isinstance(econ[k], pd.DataFrame) for k in econ))
        self.assertTrue(all(isinstance(poli[k], pd.DataFrame) for k in poli))
        self.assertTrue(os.path.exists(self.dataset.saved_data_path))
        self.test_dfs_have_quartal_and_country_columns()
        self.test_important_countries_are_included_in_both_dfs()
        self.test_amount_of_files_and_keys()
        self.test_all_files_and_keys_are_correctly_defined()
        self.test_save_dfs()

    def test_load_data_no_renewal(self):
        econ, poli = self.dataset.load_data(renew=False)
        self.assertTrue(isinstance(econ, dict))
        self.assertTrue(isinstance(poli, dict))
        self.assertTrue(all(isinstance(econ[k], pd.DataFrame) for k in econ))
        self.assertTrue(all(isinstance(poli[k], pd.DataFrame) for k in poli))
        self.assertFalse(os.path.exists(self.dataset.saved_data_path))
        self.test_dfs_have_quartal_and_country_columns()
        self.test_important_countries_are_included_in_both_dfs()
        self.test_amount_of_files_and_keys()
        self.test_all_files_and_keys_are_correctly_defined()
        
if __name__ == '__main__':
    unittest.main()
