import os
import numpy as np

def get_y_and_y_control_means(y, y_control, policies):
    means = {}
    y_means, y_control_means = [], []

    policies = [os.path.splitext(p)[0] for p in policies]

    for p in policies:
        y_mean = y.loc[:, f'2020-Q1-{p}':f'2021-Q1-{p}'].mean(axis=1)
        y_mean_name = p + '-2020-Q1_2021-Q1'
        y_mean.name = y_mean_name
        means[y_mean_name] = y_mean

        y_means.append(y_mean)

        y_control_mean = y_control.loc[:, f'2018-Q1-{p}':f'2019-Q4-{p}'].mean(axis=1)
        y_control_mean_name = p + '-2018-Q1_2019-Q4'
        y_control_mean.name = y_control_mean_name
        means[y_control_mean_name] = y_control_mean

        y_control_means.append(y_control_mean)
    return means, y_means, y_control_means

def calc_score_given_differences(diffs):
    scores = []
    for key, diff in diffs.items():
        neg_idx = diff < 0
        pos_idx = diff > 0
        total = np.abs(diff).sum()
        pos_percentage = diff[pos_idx].sum() / total
        neg_percentage = np.abs(diff)[neg_idx].sum() / total

        if 'positive' in key:
            scores.append(pos_percentage)
        elif 'negative' in key:
            scores.append(neg_percentage)
    score = np.array(scores).mean()
    return score

def get_y_means_and_y_control_means_score(y_means, y_control_means, effects_dict):
    differences = {}

    assert len(effects_dict.keys()) == len(y_means)
    assert len(effects_dict.keys()) == len(y_control_means)

    for i, effects in enumerate(effects_dict.items()):
        ind, effect = effects
        diff = (y_means[i] - y_control_means[i]).to_numpy()
        key = ind + '_' + effect
        differences[key] = diff

    score = calc_score_given_differences(differences)

    return differences,score

def calc_economic_benefit(y, y_control, dataset):
    means_dict, y_means, y_control_means= get_y_and_y_control_means(y, y_control, dataset.econ.keys())
    differences, score = get_y_means_and_y_control_means_score(y_means, y_control_means, dataset.econ_effects)
    return score



