
# The economic effects of anti-covid government policies

- Our goal is to study the effects of anti-covid government policies in the economy using Gaussian Processes.
- This code was written for the final project of the Advanced Machine Learning lecture hosted by Prof. Dr. Üllrich Köthe at the University of Heidelberg during the summer term 2021.
- This project was created by Marcos Carbonell and Johannes Kröner



