\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction (Marcos Carbonell)}{2}%
\contentsline {subsection}{\numberline {1.1}Motivation}{2}%
\contentsline {subsection}{\numberline {1.2}Available Data}{2}%
\contentsline {subsubsection}{\numberline {1.2.1}Economics}{2}%
\contentsline {subsubsection}{\numberline {1.2.2}Policies}{2}%
\contentsline {section}{\numberline {2}Methods (Johannes Kröner)}{3}%
\contentsline {section}{\numberline {3}Data Schema (Marcos Carbonell)}{4}%
\contentsline {subsection}{\numberline {3.1}Preprocessing}{4}%
\contentsline {subsubsection}{\numberline {3.1.1}Economic Data}{5}%
\contentsline {subsubsection}{\numberline {3.1.2}Policy Data}{5}%
\contentsline {subsection}{\numberline {3.2}Handling Missing Data}{5}%
\contentsline {subsubsection}{\numberline {3.2.1}Economic Data}{5}%
\contentsline {subsubsection}{\numberline {3.2.2}Policy Data}{5}%
\contentsline {subsection}{\numberline {3.3}The Dataset Class}{5}%
\contentsline {subsubsection}{\numberline {3.3.1}Unit Tests and TDD}{6}%
\contentsline {section}{\numberline {4}Building the Model (Marcos Carbonell)}{6}%
\contentsline {subsection}{\numberline {4.1}The Model Class}{6}%
\contentsline {subsection}{\numberline {4.2}Finding the Best Kernel}{6}%
\contentsline {subsection}{\numberline {4.3}Measuring Positive and Negative Economic effects}{8}%
\contentsline {subsection}{\numberline {4.4}Sampling Economic Data from Policies}{10}%
\contentsline {section}{\numberline {5}Experiments (Johannes Kröner)}{11}%
\contentsline {subsection}{\numberline {5.1}Economic impact of policies}{12}%
\contentsline {subsubsection}{\numberline {5.1.1}Discussion}{12}%
\contentsline {subsection}{\numberline {5.2}Best combination of policies}{14}%
\contentsline {subsubsection}{\numberline {5.2.1}Discussion}{16}%
\contentsline {section}{\numberline {6}Conclusion}{17}%
\contentsline {subsection}{\numberline {6.1}Johannes Kröner}{17}%
\contentsline {subsection}{\numberline {6.2}Marcos Carbonell}{17}%
